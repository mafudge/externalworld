# External World

## Table Of Contents

- [What Is This?](#What-Is-This?)
- [How To Connect](#How-To-Connect)
- [Databases Catalog](#Databases-Catalog)
- [Unstructured Data Catalog](#Unstructured-Data-Catalog)

## What is This?

The **External World** is a phrase coined by Bill Inmon, the father of data warehousing. The External World is the world outside of the data warehouse. It is the world of structured and unstructured business data. This data is considered immutable because once something happens it cannot "unhappen"

The External World consists of systems of record which drive the business. For example, they control inventory, process sales and orders, register students, play videos, and track customer interactions. Some of these systems are "off the shelf" like salesforce or peoplesoft, whereas others might be home grown. 

The external world "feeds" the data warehouse to create, as Inmon says, "a single version of the truth" for which the business can make decisions. The purpose of the data warehouse is reporting and analytics.

This repository contains a collection of databases and data sets (unstructured data) that can be used as external world systems. We will use some of these databases to practice data warehousing and business intelligence. The databases are hosted on Microsoft SQL Server and the data sets are found on S3-compatible object storage.

## How To Connect

This section explains how to connect to the external world databases and data sets. 

- Use the SQL Server and Minio S3-compatible object storage connections to profile the data. This is a convenient method to explore the data and not incur any cloud costs.
- Use the Azure Blob connections to load the data into a cloud data warehouse like Snowflake, Redshift or Azure Synapse Analytics.

### Databases on Microsoft SQL Server

Databases found in the [Databases Catalog](#Databases-Catalog) section are accessible on Microsoft SQL Server. 

You can connect to the server using the following credentials.

Server: `externalworld.cent-su.org`  
Port: `11433`  
Username: `readonlyuser`   
Password: `rYsS2LBAX8xyMvJDR6jtr`  
Encrypt: `False`  

The recommended tool to connect to the database is [Azure Data Studio (download)](https://learn.microsoft.com/en-us/sql/azure-data-studio/download-azure-data-studio). 

Here's a screenshot of the connection details:
![Azure Data Studio Connection](img/ads-connection.png)


### Minio S3-compatible Object Storage

The non-structured datasets can be found on Minio S3-compatible object storage.

You can connect to the server using the following credentials.

Server: `externalworld.cent-su.org`  
Port: `19001`  
Username: `readonlyuser`   
Password: `rYsS2LBAX8xyMvJDR6jtr`  

Use any web browser to explore the object store. [http://externalworld.cent-su.org:19001](http://externalworld.cent-su.org:19001) 

### Azure Blob Storage

Azure Blob Storage contains the same data found in SQL Server and Minio S3. It its available as cloud-native storage for easy import into a cloud data warehouse.

The Azure Blob is accessible without credentials. No need to login.

Accessing the data:
- SQL Database tables are stored as Parquet files, in the `database` container under the filename `database.table.parquet`.   
For example, the `customers` table in the `Northwind` database is available at the following url: 
[https://externalworld.blob.core.windows.net/database/northwind.customers.parquet](https://externalworld.blob.core.windows.net/database/northwind.customers.parquet)
- If there is a space in the table name, replace it with an underscore. For example, https://externalworld.blob.core.windows.net/database/northwind.order_details.parquet
- Unstructured Minio S3-compatible datasets are `files` container. The data is organized by data source.   
For example, the `fudgemart_customer_survey.csv` file is stored in the `fudgemart` folder is available at the following url:
https://externalworld.blob.core.windows.net/files/fudgemart.fudgemart_customer_survey.csv 


**IMPORTANT:** You cannot browse the Azure Blob Storage in a web browser. Therefore you must know the exact url of the file you want to access. Use SQL Server and Minio S3 to explore the data and find the exact url.


## Databases Catalog

Here is the list of databases available on Microsoft SQL Server.

### Adventureworks

AdventureWorks database supports standard online transaction processing scenarios for a fictitious bicycle manufacturer - Adventure Works Cycles. Scenarios include Manufacturing, Sales, Purchasing, Product Management, Contact Management, and Human Resources.

Schema:  
[https://stackoverflow.com/questions/37190921/understanding-adventureworks2012-db-structure](https://stackoverflow.com/questions/37190921/understanding-adventureworks2012-db-structure)

### AdventureworksDW

A Kimball data warehouse containing the AdventureWorks data. Use this as a source for understanding the Kimball data warehousing and business intelligence methodologies.

Schema:  
[https://dataedo.com/samples/html/Data_warehouse/doc/AdventureWorksDW_4/modules.html](https://dataedo.com/samples/html/Data_warehouse/doc/AdventureWorksDW_4/modules.html)

### Northwind 

The Northwind database contains sales data for a fictitious company called "Northwind Traders," which imports and exports specialty foods from around the world.

![Northwind](img/northwind.png)


### Fudgemart (v3)

A Simple database for a fictional brick and mortar company called FudgeMart. The database is used to track sales orders, customers, and products. 

![Fudgemart](img/fudgemart-v3.png)

### Fudgeflix (v3)

A database based on the Neflix DVD-by mail rental service. The database is used to track customers, movies, and movie rentals, and billing.

![Fudgeflix](img/fudgeflix-v3.png)

### Chinook

The Chinook database is an "iTunes" clone. It represents a digital media store, including tables for artists, albums, media tracks, playlists, invoices and customers.

![Chinook](img/chinook.png)

### Pubs

The pubs database is a classic set of information about book publishing. It contains tables for authors, titles, sales, and publishers as well as bookstores and sales.

![Pubs](img/pubs.png)

### SampleU

This is an example university registrar database. It contains tables for courses, and enrollment, as well as students and instructors.

![SampleU](img/sampleU.png)



### StackOverflow

The stackoverflow database is a clone of the StackOverflow.com forums. It contains tables for users, posts, comments, and votes, and its an actual snapshot of the data from the StackOverflow.com site.

Schema:  
[https://meta.stackexchange.com/questions/2677/database-schema-documentation-for-the-public-data-dump-and-sede](https://meta.stackexchange.com/questions/2677/database-schema-documentation-for-the-public-data-dump-and-sede)

### vBay!

The vBay! database is an E-Bay! clone. It contains tables for users, auctions, bids, and categories, and ratings.

![vBay](img/vbay.png)


## Unstructured Data

Here are the list of files on Minio S3 Object storage.

## Fudgemart

- `fudgemart-tweets.json` - Sample Twitter API output in JSON format of customers tweeting about Fudgemart.
- `fudgemart_customer_survey.csv` - Sample customer survey extract in CSV format. Contains demographic information about customers.

## Northwind 

- `NorthwindDailyInventoryLevelsOneWeek.csv` - Sample inventory extract in CSV format. Contains daily inventory levels for one week.

## SampleU

- `course-evals.json` - Course evaluations in JSON format. Contains student feedback about courses and instructors.

## Pubs

- `1995orders.csv` - Order for the year 1995 in CSV format. Contains order information for books.
- `1996orders.csv` - Order for the year 1995 in CSV format. Contains order information for books.

## Chinook

- `customer-track-reviews.json` - Customer reviews of various tracks in the chinook database, expressed as "like" or "dislike". Also some customers reviewed tracks they did not own.


