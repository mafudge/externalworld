-- create login readonlyuser with password = 'Demo12345'
-- GO

use [AdventureWorks2019]
GO

create user readonlyuser for login readonlyuser 
GO
EXEC sp_addrolemember 'db_datareader', 'readonlyuser'

GO


use [AdventureWorksDW2019]
GO

create user readonlyuser for login readonlyuser 
GO
EXEC sp_addrolemember 'db_datareader', 'readonlyuser'
GO


use [fudgemart_v3]
GO

create user readonlyuser for login readonlyuser 
GO
EXEC sp_addrolemember 'db_datareader', 'readonlyuser'
GO

use [fudgeflix_v3]
GO

create user readonlyuser for login readonlyuser 
GO
EXEC sp_addrolemember 'db_datareader', 'readonlyuser'
GO


use [northwind]
GO

create user readonlyuser for login readonlyuser 
GO
EXEC sp_addrolemember 'db_datareader', 'readonlyuser'
GO


use [pubs]
GO

create user readonlyuser for login readonlyuser 
GO
EXEC sp_addrolemember 'db_datareader', 'readonlyuser'
GO



use [Chinook]
GO

create user readonlyuser for login readonlyuser 
GO
EXEC sp_addrolemember 'db_datareader', 'readonlyuser'
GO


use [sampleu]
GO

create user readonlyuser for login readonlyuser 
GO
EXEC sp_addrolemember 'db_datareader', 'readonlyuser'
GO

use [stackoverflow]
GO

create user readonlyuser for login readonlyuser 
GO
EXEC sp_addrolemember 'db_datareader', 'readonlyuser'
GO


use [vBay]
GO

create user readonlyuser for login readonlyuser 
GO
EXEC sp_addrolemember 'db_datareader', 'readonlyuser'
GO
