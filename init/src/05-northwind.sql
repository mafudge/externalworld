USE [master];
GO
RESTORE DATABASE [northwind]
FROM DISK = N'/src/backups/northwind.bak'
WITH
    MOVE 'northwind' TO '/var/opt/mssql/data/northwind.mdf',
    MOVE 'northwind_log' TO '/var/opt/mssql/data/northwind_log.ldf',
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO
