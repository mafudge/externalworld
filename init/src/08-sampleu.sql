USE [master];
GO
RESTORE DATABASE [sampleu]
FROM DISK = N'/src/backups/sampleu.bak'
WITH
    MOVE 'sampleu' TO '/var/opt/mssql/data/sampleu.mdf',
    MOVE 'sampleu_log' TO '/var/opt/mssql/data/sampleu_log.ldf',
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO
