USE [master];
GO
RESTORE DATABASE [pubs]
FROM DISK = N'/src/backups/pubs.bak'
WITH
    MOVE 'pubs' TO '/var/opt/mssql/data/pubs.mdf',
    MOVE 'pubs_log' TO '/var/opt/mssql/data/pubs_log.ldf',
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO
