USE [master];
GO
RESTORE DATABASE [fudgemart_v3]
FROM DISK = N'/src/backups/fudgemart_v3.bak'
WITH
    MOVE 'fudgemart_v3' TO '/var/opt/mssql/data/fudgemart_v3.mdf',
    MOVE 'fudgemart_v3_log' TO '/var/opt/mssql/data/fudgemart_v3_log.ldf',
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO
