USE [master];
GO
RESTORE DATABASE [Chinook]
FROM DISK = N'/src/backups/chinook.bak'
WITH
    MOVE 'chinook' TO '/var/opt/mssql/data/chinook.mdf',
    MOVE 'chinook_log' TO '/var/opt/mssql/data/chinook_log.ldf',
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO
