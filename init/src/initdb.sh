#!/bin/bash
MINIO_ALIAS=ew
echo "Waiting for services to start"
/bin/sleep ${DELAY}

echo copying over backups...
# TODO MAKE THESE SYMLINKS INSTEAD OF COPIES!!!!
cp -uv /backups/*.bak /src/backups/

echo "setting minio alias to ${MINIO_ALIAS}}"
/usr/local/bin/mc alias set ${MINIO_ALIAS} http://minio:9000 minioadmin ${ADMIN_PASSWORD}

echo "creating minio bucket policy"
/usr/local/bin/mc admin policy create ${MINIO_ALIAS} readlist /src/readlist-policy.json

echo "creating minio user ${RO_USER}"
/usr/local/bin/mc admin user add ${MINIO_ALIAS} ${RO_USER} ${RO_PASSWORD}

echo "attaching the readlist policy to the ${RO_USER} user"
/usr/local/bin/mc admin policy attach ${MINIO_ALIAS} readlist --user ${RO_USER}


echo "mirroring minio files to ${MINIO_ALIAS}"
/usr/local/bin/mc mirror /src/files/ ${MINIO_ALIAS}

# echo "downloading stackoverflow database"
# if ! [ -f /src/backups/StackOverflow2010.7z ]; then
#   /usr/bin/curl  https://downloads.brentozar.com/StackOverflow2010.7z --output /src/backups/StackOverflow2010.7z
# else
#     echo "StackOverflow2010.7z already exists"
# fi

# echo "extracting stackoverflow database"
# if ! [ -f /src/backups/StackOverflow2010.mdf ]; then
#     /usr/bin/7z x /src/backups/StackOverflow2010.7z -o/src/backups
# else
#     echo "StackOverflow2010 database already extracted"
# fi 

echo "Loading databases into MSSQL"
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/01-adventureworks-dw.sql
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/02-adventureworks-oltp.sql
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/03-fudgemart_v3.sql
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/04-fudgeflix_v3.sql
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/05-northwind.sql
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/06-pubs.sql
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/07-chinook.sql
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/08-sampleu.sql
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/09-stackoverflow.sql
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/10-vbay.sql
echo "creating readonly user"
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -Q "CREATE LOGIN readonlyuser WITH PASSWORD = '${RO_PASSWORD}'"
echo "setting user permissions on databases"
/usr/local/bin/mssql-cli -S mssql -U SA -P ${ADMIN_PASSWORD} -i /src/99-create-readonly-user.sql
