USE [master];
GO
RESTORE DATABASE [AdventureWorksDW2019]
FROM DISK = N'/src/backups/AdventureWorksDW2019.bak'
WITH
    MOVE 'AdventureWorksDW2019' TO '/var/opt/mssql/data/AdventureWorksDW2019.mdf',
    MOVE 'AdventureWorksDW2019_log' TO '/var/opt/mssql/data/AdventureWorksDW2019_log.ldf',
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO
