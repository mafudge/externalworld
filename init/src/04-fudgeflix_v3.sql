USE [master];
GO
RESTORE DATABASE [fudgeflix_v3]
FROM DISK = N'/src/backups/fudgeflix_v3.bak'
WITH
    MOVE 'fudgeflix_v3' TO '/var/opt/mssql/data/fudgeflix_v3.mdf',
    MOVE 'fudgeflix_v3_log' TO '/var/opt/mssql/data/fudgeflix_v3_log.ldf',
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO
