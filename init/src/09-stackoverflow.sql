USE [master];

-- EXEC sp_attach_db @dbname = 'stackoverflow2010',   
--     @filename1 = '/src/backups/StackOverflow2010.mdf',   
--     @filename2 = '/src/backups/StackOverflow2010_log.ldf';

GO
RESTORE DATABASE [stackoverflow] FROM 
DISK = '/src/backups/stackoverflow.bak'
WITH
    MOVE 'stackoverflow2010' TO '/var/opt/mssql/data/stackoverflow.mdf',
    MOVE 'stackoverflow2010_log' TO '/var/opt/mssql/data/stackoverflow_log.ldf',
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO