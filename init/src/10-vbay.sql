USE [master];
GO
RESTORE DATABASE [vBay]
FROM DISK = N'/src/backups/vbay.bak'
WITH
    MOVE 'vbay' TO '/var/opt/mssql/data/vbay.mdf',
    MOVE 'vbay_log' TO '/var/opt/mssql/data/vbay_log.ldf',
    FILE = 1,
    NOUNLOAD,
    STATS = 5;
GO
